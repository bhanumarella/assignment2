import pytest

from src_cal.area import area_square, area_rectangle


def test_area_square():
    assert area_square(4) == 16


def test_area_rectangle():
    assert area_rectangle(2, 4) == 8


def test_exception_with_negative_number():
    with pytest.raises(Exception):
        area_square(-8)
        area_rectangle(20,-2)

