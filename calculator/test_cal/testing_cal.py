from src_cal.calc import sum,diff


def test_sum():
    assert sum(2, 1) == 3


def test_can_add_large_numbers():
    assert sum(99999, 99999) == 2 * 99999


def test_can_add_negative_numbers():
    assert sum(-10, -10) == -20


def test_diff():
    assert diff(2, 1) == 1


def test_negative_diff():
    assert diff(1, 2) == -1


def test_diff_between_negative_numbers():
    assert diff(-20, -10) == -10
