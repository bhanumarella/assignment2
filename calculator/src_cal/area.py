def area_square(radius):
    if radius < 0:
        raise Exception("Negative number")
    return radius * radius


def area_rectangle(length, breadth):
    if length < 0 or breadth < 0:
        raise Exception("Negative number")
    return length * breadth
