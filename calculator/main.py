from src_cal.area import area_square, area_rectangle
from src_cal.calc import diff, sum

print("Enter radius of the square")
radius = int(input())
print("area of square is", area_square(radius))

print("Enter length and breadth of the rectangle")
length = int(input())
breadth = int(input())
print("area of square is", area_rectangle(length, breadth))

print("Enter two numbers for addition and subtraction")
x = int(input())
y = int(input())

print("Sum is : ", sum(x,y))
print("Diff is : ", diff(x,y))